#include <iostream>
#include <string>
using namespace std;

int main() {
    string s;
    int number_of_entries = 0;
    int index = -2;
    
    cin >> s;

    for (int i=0; i<s.size(); i++)
    {
	if (s[i] == 'f')
	{
	    ++number_of_entries;
	    if (number_of_entries == 1)
	    {
		index = -1;
	    }
	    else
	    {
		index = i;
		break;
	    }
	}
    }
    cout << index;
}
