#include <iostream>

using namespace std;

int Factorial(int x)
{
    if (x <= 0)
    {
	return 1;
    }
    else
    {
	return Factorial(x-1)*x;
    }
}

/*int main() {
    int x;
    cin >> x;
    cout << Factorial(x);
    
    }*/
