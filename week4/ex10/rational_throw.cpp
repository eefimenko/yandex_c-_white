#include <iostream>
#include <exception>
#include <math.h>
using namespace std;

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
	num = 0;
	den = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
	if (denominator == 0)
	{
	    throw invalid_argument("fail");
	}
	if (numerator != 0)
	{
	    int c = fabs(denominator);
	    int a = fabs(numerator);
	    int b = c;
	    
	    while (a > 0 && b > 0) {
		if (a > b) {
		    a %= b;
		} else {
		    b %= a;
		}
	    }
	    int nod = a + b;
	    
	    num = numerator*denominator/(c*nod);
	    den = c/nod;
	}
	else
	{
	    num = 0;
	    den = 1;
	}
    }
     

    int Numerator() const {
        // Реализуйте этот метод
	return num;
    }

    int Denominator() const {
        // Реализуйте этот метод
	return den;
    }

private:
    // Добавьте поля
    int num;
    int den;
};

Rational operator / (const Rational& lhs, const Rational& rhs)
{
    if (rhs.Numerator() == 0)
    {
	throw domain_error("fail");
    }
    int a = lhs.Numerator() * rhs.Denominator();
    int b = lhs.Denominator() * rhs.Numerator();
    return Rational(a,b);
}


int main() {
    try {
        Rational r(1, 0);
        cout << "Doesn't throw in case of zero denominator" << endl;
        return 1;
    } catch (invalid_argument& ex) {
    }

    try {
        auto x = Rational(1, 2) / Rational(0, 1);
        cout << "Doesn't throw in case of division by zero" << endl;
        return 2;
    } catch (domain_error& ex) {
    }

    cout << "OK" << endl;
    return 0;
}
