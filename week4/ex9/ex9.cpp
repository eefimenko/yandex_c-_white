#include <string>
#include <iostream>
#include <sstream>
using namespace std;

void EnsureEqual(const string& left, const string& right)
{
    if (left != right)
    {
	stringstream ss;
	ss << left << " != " << right;
	throw runtime_error(ss.str());
    }
}

/*int main() {
    string s1 = "abc";
    string s2 = "abc";
    string s3 = "def";
    try
    {
	EnsureEqual(s1,s2);
    }
    catch (exception& ex)
    {
	cout << ex.what() << endl;
    }
    try
    {
	EnsureEqual(s1,s3);
    }
    catch (exception& ex)
    {
	cout << ex.what() << endl;
    }
       
    }*/

