#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

void MoveStrings(vector<string>& source, vector<string>& destination)
{
    for (auto w: source)
    {
	destination.push_back(w);
    }
    source.clear();
}

/*int main() {
    vector<string> a = {"a", "b"};
    vector<string> b = {"a", "b"};
    MoveStrings(a, b);
    cout << "Source " << endl;
    for (auto w: a)
    {
	cout << w << endl;
    }
    cout << "Destination " << endl;
    for (auto w: b)
    {
	cout << w << endl;
    }
    }*/
