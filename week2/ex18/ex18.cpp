#include <iostream>
#include <string>
#include <map>
#include <set>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    map<set<string>, int> bus; // stops
        
    int number;
    
    for (int i = 0; i < N; i++)
    {
	cin >> number;
	set<string> s;
	
	for (int k = 0; k < number; k++)
	{
	    string stop;
	    cin >> stop;
	    s.insert(stop);
	}
	
	if (bus.count(s) > 0)
	{
	    int i = bus.find(s)->second;
	    cout << "Already exists for " << i << endl;
	}
	else
	{
	    int i = bus.size() + 1;
	    bus[s] = i;
	    cout << "New bus " << i << endl;
	}
    }
}
