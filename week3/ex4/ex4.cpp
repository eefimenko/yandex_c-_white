#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Person {
public:
    void ChangeFirstName(int year, const string& first_name) {
	name[year] = first_name;
    }
    void ChangeLastName(int year, const string& last_name) {
	surname[year] = last_name;
    }
    string GetFullName(int year) {
	string full_name, name = "", surname = "";
	auto it = this->name.upper_bound(year);
	if (it != begin(this->name))
	{
	    it--;
	    name = it->second;; 
	}
	it = this->surname.upper_bound(year);
	if (it != begin(this->surname))
	{
	    it--;
	    surname = it->second; 
	}
	if (name == "" && surname == "")
	{
	    full_name = "Incognito";
	}
	else if (name == "" && surname != "")
	{
	    full_name = surname + " with unknown first name";
	}
	else if (name != "" && surname == "")
	{
	    full_name = name + " with unknown last name";
	}
	else
	{
	    full_name = name + " " + surname;
	}
	return full_name;
    }
private:
    map<int,string> name;
    map<int,string> surname;
};

/*int main() {
    Person person;
  
    person.ChangeFirstName(1965, "Polina");
    person.ChangeLastName(1967, "Sergeeva");
    for (int year : {1900, 1965, 1990}) {
	cout << person.GetFullName(year) << endl;
    }
  
    person.ChangeFirstName(1970, "Appolinaria");
    for (int year : {1969, 1970}) {
	cout << person.GetFullName(year) << endl;
    }
  
    person.ChangeLastName(1968, "Volkova");
    for (int year : {1969, 1970}) {
	cout << person.GetFullName(year) << endl;
    }
  
    return 0;
    } */ 

