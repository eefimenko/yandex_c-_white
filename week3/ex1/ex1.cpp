#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    int N;
    cin >> N;
    vector<int> vec(N);
    for (auto& i : vec)
    {
	cin >> i;
    }
    
    sort(begin(vec), end(vec), [](int x, int y) {if (abs(x) < abs(y)) {return true;} return false; });

    for (const auto& i : vec)
    {
	cout << i << " ";
    }
    cout << endl;
}
