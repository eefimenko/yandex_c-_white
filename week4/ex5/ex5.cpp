#include <fstream>
#include <string>
#include <iomanip>
#include <iostream>
using namespace std;

int main() {
    ifstream input("input.txt");
    double temp;
    cout << fixed;
    cout << setprecision(3);
    while (input >> temp)
    {
	cout << temp << endl;
    }
    return 0;
}

