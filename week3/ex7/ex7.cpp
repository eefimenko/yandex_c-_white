#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Person {
public:
    Person(const string& birth_name, const string& birth_surname, int birth_year)
    {
	byear = birth_year;
	name[birth_year] = birth_name;
	surname[birth_year] = birth_surname;
    }
    
    void ChangeFirstName(int year, const string& first_name) {
	if (year < byear)
	{
	    return;
	}
	name[year] = first_name;
    }
    
    void ChangeLastName(int year, const string& last_name) {
	if (year < byear)
	{
	    return;
	}
	surname[year] = last_name;
    }
    
    string GetFullName(int year) const {
	if (year < byear)
	{
	    return "No person";
	}
	
	string full_name,
	    name = FindName(year),
	    surname = FindSurname(year);
	
	if (name == "" && surname == "")
	{
	    full_name = "Incognito";
	}
	else if (name == "" && surname != "")
	{
	    full_name = surname + " with unknown first name";
	}
	else if (name != "" && surname == "")
	{
	    full_name = name + " with unknown last name";
	}
	else
	{
	    full_name = name + " " + surname;
	}
	return full_name;
    }
    string GetFullNameWithHistory(int year) const {
	if (year < byear)
	{
	    return "No person";
	}
	
	string full_name,
	    name = FindNameWithHistory(year),
	    surname = FindSurnameWithHistory(year);
	
	if (name == "" && surname == "")
	{
	    full_name = "Incognito";
	}
	else if (name == "" && surname != "")
	{
	    full_name = surname + " with unknown first name";
	}
	else if (name != "" && surname == "")
	{
	    full_name = name + " with unknown last name";
	}
	else
	{
	    full_name = name + " " + surname;
	}
	return full_name;
    }
private:
    map<int,string> name;
    map<int,string> surname;
    int byear;
    
    string FindName(int year) const 
    {
	string name = "";
	auto it = this->name.upper_bound(year);
	if (it != begin(this->name))
	{
	    it--;
	    name = it->second;; 
	}
	return name;
    }
    string FindSurname(int year) const
    {
	string surname = "";
	auto it = this->surname.upper_bound(year);
	if (it != begin(this->surname))
	{
	    it--;
	    surname = it->second;; 
	}
	return surname;
    }
    
    string FindNameWithHistory(int year) const
    {
	string name = "";
	vector<string> history;

	if (this->name.empty())
	{
	    return name;
	}

	string prev_name = name;
	for (const auto& s: this->name)
	{
	    if (s.first <= year)
	    {
		if (s.second != prev_name)
		{
		    prev_name = s.second;
		    history.push_back(s.second);
		}
	    }
	    else
	    {
		break;
	    }
	}
	
	name = prev_name;
	
	if (history.size() > 1)
	{
	    name += " (";
	    for (int k=history.size()-2; k > 0; k--)
	    {
		name += history[k] + ", ";
	    }
	    name += history[0] + ")";
	}
	return name;
    }

    string FindSurnameWithHistory(int year) const
    {
	string surname = "";
	vector<string> history;

	if (this->surname.empty())
	{
	    return surname;
	}

	string prev_surname = surname;
	for (const auto& s: this->surname)
	{
	    if (s.first <= year)
	    {
		if (s.second != prev_surname)
		{
		    prev_surname = s.second;
		    history.push_back(s.second);
		}
	    }
	    else
	    {
		break;
	    }
	}
	
	surname = prev_surname;
	
	if (history.size() > 1)
	{
	    surname += " (";
	    for (int k=history.size()-2; k > 0; k--)
	    {
		surname += history[k] + ", ";
	    }
	    surname += history[0] + ")";
	}
	return surname;
    }
};

/*int main() {
    
    Person person("Polina", "Sergeeva", 1960);
    for (int year : {1959, 1960}) {
	cout << person.GetFullNameWithHistory(year) << endl;
    }
  
    person.ChangeFirstName(1965, "Appolinaria");
    person.ChangeLastName(1967, "Ivanova");
    for (int year : {1965, 1967}) {
	cout << person.GetFullNameWithHistory(year) << endl;
    }    
   
   
    return 0;
    }*/

