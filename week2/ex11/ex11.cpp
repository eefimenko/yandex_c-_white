#include <iostream>
#include <string>
#include <map>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;

    for (int i = 0; i < N; i++)
    {
	string s1,s2;
	cin >> s1 >> s2;
	map<char,int> dict1, dict2;
	for (auto c: s1 )
	{
	    dict1[c]++;
	}
	for (auto c: s2 )
	{
	    dict2[c]++;
	}
	if (dict1 == dict2)
	{
	    cout << "YES" << endl;
	}
	else
	{
	    cout << "NO" << endl;
	}
    }
    
}
