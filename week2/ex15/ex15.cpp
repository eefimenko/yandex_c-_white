#include <iostream>
#include <set>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    set<string> words; // stops
        
    string word;
    
    for (int i = 0; i < N; i++)
    {
	cin >> word;
	words.insert(word);
    }
    cout << words.size() << endl;
}
