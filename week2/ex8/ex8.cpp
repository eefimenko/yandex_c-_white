#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
    int N;
    cin >> N;
    vector<int> t(N);
    vector<int> days;
    
    for (int i=0; i<N; ++i)
    {
	cin >> t[i];
    }

    int t_av = 0;
    for (auto& n: t)
    {
	t_av += n;
    }
    t_av /= N;

    for (int i=0; i<N; i++)
    {
	if (t[i] > t_av)
	{
	    days.push_back(i);
	}
    }
    cout << days.size() << endl;
    for (auto& d: days)
    {
	cout << d << " ";
    }
    cout << endl;
}
