#include <fstream>
#include <string>
#include <iomanip>
#include <iostream>
using namespace std;

int main() {
    ifstream input("input.txt");
    int row, column;
    string value;
    
    input >> row >> column;
    getline(input, value, '\n');
//    cout << row << " " << column << endl;

    for (int i=0; i<row; i++)
    {
	for (int j=0; j<column-1; j++)
	{
	    getline(input, value, ',');
	    cout << setw(10);
	    cout << value << " ";
	}
	getline(input, value, '\n');
	cout << setw(10);
	cout << value << endl;
    }
    return 0;
}

