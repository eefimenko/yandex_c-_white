#include "date.h"
#include <iostream>
#include <string>
#include <map>
#include <set>
#include <sstream>
#include <iomanip>

using namespace std;
// Реализуйте функции и методы классов и при необходимости добавьте свои

void PeekAndSkip(istream& in, const string& s)
{
    char peek = in.peek(); 
    if (peek != '-')
    {
	throw domain_error(s);
    }
    in.ignore(1);
}

void PeekAndSkipLast(istream& in, const string& s)
{
    char peek = in.peek(); 
    if (peek != ' ' && peek != EOF)
    {
	throw domain_error(s);
    }
    in.ignore(1);
}

class Date {
public:
    Date(int day_=1, int month_=1, int year_=0)
    {
	if (month_ < 1 || month_ > 12)
	{
	    stringstream ss;
	    ss << "Month value is invalid: " << month_;
	    throw invalid_argument(ss.str());
	}
	if (day_ < 1 || day_ > 31)
	{
	    stringstream ss;
	    ss << "Day value is invalid: " << day_;
	    throw invalid_argument(ss.str());
	}
	day = day_;
	month = month_;
	year = year_;
    }
    
    Date(const string& s)
    {
	string message = "Wrong date format: " + s;
	stringstream ss(s);
	int year_, month_, day_;
	ss >> year_;
	PeekAndSkip(ss, message);
        ss >> month_;
	if (month_ < 1 || month_ > 12)
	{
	    stringstream ss;
	    ss << "Month value is invalid: " << to_string(month_);
	    throw invalid_argument(ss.str());
	}
	PeekAndSkip(ss, message);
	ss >> day_;
	if (day_ < 1 || day_ > 31)
	{
	    stringstream ss;
	    ss << "Day value is invalid: " << to_string(day_);
	    throw invalid_argument(ss.str());
	}
	PeekAndSkipLast(ss, message);
	day = day_;
	month = month_;
	year = year_;
    }
    
    int GetYear() const
    {
	return year;
    };
    int GetMonth() const
    {
	return month;
    };
    int GetDay() const
    {
	return day;
    };

    // convert into a number of days since epoch
    int GetNumberOfDays() const
    {
	return year*12*31 + (month-1)*31 + day;	
    }

    string toString() const
    {
	stringstream ss;
	ss << setw(4) << setfill('0') << year << "-"
	   << setw(2) << setfill('0') << month << "-"
	   << setw(2) << setfill('0') << day;
	return ss.str();
    }
    
private:
    int year;
    int month;
    int day;
};

bool operator<(const Date& lhs, const Date& rhs)
{
    return lhs.GetNumberOfDays() < rhs.GetNumberOfDays();
};


class Database {
public:
    void AddEvent(const Date& date, const string& event)
    {
	storage[date].insert(event);
    };
    bool DeleteEvent(const Date& date, const string& event)
    {
	if (storage[date].count(event) > 0)
	{
	    storage[date].erase(event);
	    return true;
	}
	return false;
    };
    int  DeleteDate(const Date& date)
    {
	int numberOfEvents = storage[date].size();
	storage.erase(date);
	return numberOfEvents;
    };

    set<string> Find(const Date& date) const
    {
	set<string> m;
	if (storage.count(date) > 0)
	{
	    m = storage.at(date);
	}
	return m;
    };
  
    void Print() const
    {
	for (const auto& p: storage)
	{
	    string date = p.first.toString();
	    for (const auto& v: p.second)
	    {
		cout << date << " " << v << endl;
	    }
	}
    };
private:
    map<Date,set<string>> storage;
};

int main() {
  Database db;
    
  string command;
  while (getline(cin, command)) {
      stringstream ss(command);
      string operation;
      ss >> operation;
      try {
	  if (operation == "Add")
	  {
	      string event,date_s;
	      ss.ignore(1);
	      getline(ss, date_s, ' ');
	      Date date(date_s);
	      ss >> event;
	      if (event == "")
	      {
		  return -1;
	      }
	      db.AddEvent(date, event);
	  }
	  else if (operation == "Del")
	  {
	      string event,date_s;
	      ss.ignore(1);
	      getline(ss, date_s, ' ');
	      Date date(date_s);
	      ss >> event;
	      if (event == "")
	      {
		  int n = db.DeleteDate(date);
		  cout << "Deleted " << n << " events" << endl; 
	      }
	      else
	      {
		  if (db.DeleteEvent(date, event))
		  {
		      cout << "Deleted successfully" << endl;
		  }
		  else
		  {
		      cout << "Event not found" << endl;
		  }
	      }
	  }
	  else if (operation == "Find")
	  {
	      string date_s;
	      ss.ignore(1);
	      getline(ss, date_s, ' ');
	      Date date(date_s);
	      set<string> res = db.Find(date);
	      for (const auto& s: res)
	      {
		  cout << s << endl;
	      }
	  }
	  else if (operation == "Print")
	  {
	      db.Print();
	  }
	  else
	  {
	      if (operation != "")
	      {
		  cout << "Unknown command: " << operation << endl;
	      }
	  }
      }
      catch (invalid_argument& ex)
      {
	  cout << ex.what() << endl;
	  return -1;
      }
      catch (domain_error& ex)
      {
	  cout << ex.what() << endl;
	  return -1; 
      }
      catch (exception& ex)
      {
	  return -1; 
      }
  }

  return 0;
}
