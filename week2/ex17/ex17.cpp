#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    set<vector<string>> words; // stops
    map<string,int> number;    
    string command;
    
    for (int i = 0; i < N; i++)
    {
	cin >> command;
	if (command == "ADD")
	{
	    vector<string> pair(2);
	    for (auto& s: pair)
	    {
		cin >> s;
	    }
	    sort(pair.begin(), pair.end());
	    if (words.count(pair) == 0)
	    {
		number[pair[0]]++;
		number[pair[1]]++;
		words.insert(pair);
	    }
	}
	else if (command == "CHECK")
	{
	    bool is_syn = false;
	    vector<string> pair(2);
	    for (auto& s: pair)
	    {
		cin >> s;
	    }
	    sort(pair.begin(), pair.end());

	    if (words.count(pair) > 0)
	    {
		is_syn = true;
	    }
	    
	    if (is_syn)
	    {
		cout << "YES" << endl;
	    }
	    else
	    {
		cout << "NO" << endl;
	    }
	}
	else if (command == "COUNT")
	{
	    string word;
	    cin >> word;
	    
	    cout << number[word] << endl;
	}
	
    }
}
