#include <iostream>
#include <math.h>
using namespace std;

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
	num = 0;
	den = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
	if (numerator != 0)
	{
	    int c = fabs(denominator);
	    int a = fabs(numerator);
	    int b = c;
	    
	    while (a > 0 && b > 0) {
		if (a > b) {
		    a %= b;
		} else {
		    b %= a;
		}
	    }
	    int nod = a + b;
	    
	    num = numerator*denominator/(c*nod);
	    den = c/nod;
	}
	else
	{
	    num = 0;
	    den = 1;
	}
    }
     

    int Numerator() const {
        // Реализуйте этот метод
	return num;
    }

    int Denominator() const {
        // Реализуйте этот метод
	return den;
    }

private:
    // Добавьте поля
    int num;
    int den;
};

// Реализуйте для класса Rational операторы ==, + и -
bool operator == (const Rational& lhs, const Rational& rhs)
{
    return (lhs.Numerator() == rhs.Numerator() && lhs.Denominator() == rhs.Denominator());
}

Rational operator - (const Rational& lhs, const Rational& rhs)
{
    int a = lhs.Numerator() * rhs.Denominator() - rhs.Numerator() * lhs.Denominator();
    int b = lhs.Denominator() * rhs.Denominator();
    return Rational(a,b);
}

Rational operator + (const Rational& lhs, const Rational& rhs)
{
    int a = lhs.Numerator() * rhs.Denominator() + rhs.Numerator() * lhs.Denominator();
    int b = lhs.Denominator() * rhs.Denominator();
    return Rational(a,b);
}

Rational operator * (const Rational& lhs, const Rational& rhs)
{
    int a = lhs.Numerator() * rhs.Numerator();
    int b = lhs.Denominator() * rhs.Denominator();
    return Rational(a,b);
}

Rational operator / (const Rational& lhs, const Rational& rhs)
{
    int a = lhs.Numerator() * rhs.Denominator();
    int b = lhs.Denominator() * rhs.Numerator();
    return Rational(a,b);
}

int main() {
    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a * b;
        bool equal = c == Rational(8, 9);
        if (!equal) {
            cout << "2/3 * 4/3 != 8/9" << endl;
            return 1;
        }
    }

    {
        Rational a(5, 4);
        Rational b(15, 8);
        Rational c = a / b;
        bool equal = c == Rational(2, 3);
        if (!equal) {
            cout << "5/4 / 15/8 != 2/3" << endl;
            return 2;
        }
    }

    cout << "OK" << endl;
    return 0;
}
