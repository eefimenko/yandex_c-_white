#include <iostream>
#include <string>
#include <map>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    map<string,string> dict;
    string command;
    
    for (int i = 0; i < N; i++)
    {
	cin >> command;
	if (command == "CHANGE_CAPITAL")
	{
	    string country, capital;
	    cin >> country >> capital;
	    if (dict.count(country) == 0) // country does not exist yet
	    {
		cout << "Introduce new country " << country << " with capital " << capital << endl;
	    }
	    else if (dict[country] == capital)
	    {
		cout << "Country " << country << " hasn't changed its capital" << endl;
	    }
	    else
	    {
		cout << "Country " << country << " has changed its capital from " << dict[country] << " to " << capital << endl;
	    }
	    dict[country] = capital;	
	}
	else if (command == "RENAME")
	{
	    string old_country, new_country;
	    cin >> old_country >> new_country;
	    
	    if (old_country == new_country || dict.count(old_country) == 0 || dict.count(new_country) > 0)
	    {
		cout << "Incorrect rename, skip" << endl;
	    }
	    else
	    {
		cout << "Country " << old_country << " with capital " << dict[old_country] << " has been renamed to " << new_country << endl;
		dict[new_country] = dict[old_country];
		dict.erase(old_country);
	    }
	}
	else if (command == "ABOUT")
	{
	    string country;
	    cin >> country;
	    if (dict.count(country) == 0)
	    {
		cout << "Country " << country << " doesn't exist" << endl;
	    }
	    else
	    {
		cout << "Country " << country << " has capital " << dict[country] << endl;
	    }
	}
	else if (command == "DUMP")
	{
	    if (dict.size() == 0)
	    {
		cout << "There are no countries in the world" << endl;
	    }
	    else
	    {
		for (auto p: dict)
		{
		    cout << p.first << "/" << p.second << " ";
		}
		cout << endl;
	    }
	}
	
    }
    
}
