#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    map<string,vector<string>> stops_for_buses; // stops
    map<string,vector<string>> buses_for_stops; // buses
    
    string command;
    
    for (int i = 0; i < N; i++)
    {
	cin >> command;
	if (command == "NEW_BUS")
	{
	    string bus, stop;
	    int number_of_stops;
	    cin >> bus >> number_of_stops;

	    for (int k = 0; k < number_of_stops; k++)
	    {
		cin >> stop;
		stops_for_buses[bus].push_back(stop);
		buses_for_stops[stop].push_back(bus);
	    }
	}
	else if (command == "BUSES_FOR_STOP")
	{
	    string stop;
	    cin >> stop;
	    	    
	    if (buses_for_stops.count(stop) > 0)
	    {
		for (const auto& b: buses_for_stops[stop])
		{
		    cout << b << " ";
		}
		cout << endl;
	    }
	    else
	    {
		cout << "No stop" << endl;
	    }
	}
	else if (command == "STOPS_FOR_BUS")
	{
	    string bus;
	    cin >> bus;
	    if (stops_for_buses.count(bus) > 0)
	    {
		for (auto& s: stops_for_buses[bus])
		{
		    cout << "Stop " << s << ":";
		    if (buses_for_stops[s].size() > 1)
		    {
			for (const auto& b: buses_for_stops[s])
			{
			    if (b != bus)
			    {
				cout << " " << b;
			    }
			}
			cout << endl;
		    }
		    else if (buses_for_stops[s].size() == 1)
		    {
			cout << " no interchange" << endl;
		    }
		}
	    }
	    else
	    {
		cout << "No bus" << endl;
	    }
	}
	else if (command == "ALL_BUSES")
	{
	    if (stops_for_buses.size() == 0)
	    {
		cout << "No buses" << endl;
	    }
	    else
	    {
		for (const auto& p: stops_for_buses)
		{
		    const string& bus = p.first;
		    const vector<string>& stops = p.second;
		    cout << "Bus " << bus << ":";
		    for (const auto& s: stops)
		    {
			cout << " " << s;
		    }
		    cout << endl;
		}
	    }
	}
	
    }
    
}
