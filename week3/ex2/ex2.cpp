#include <iostream>
#include <vector>
#include <algorithm>
#include <locale>
#include <string>

using namespace std;

int main() {
    int N;
    cin >> N;
    vector<string> vec(N);
    for (auto& i : vec)
    {
	cin >> i;
    }
    
    sort(begin(vec), end(vec), [](const string& x, const string& y) {
	    string x1 = x;
	    string y1 = y;
	    for (auto& c : x1)
	    {
		c = tolower(c);
	    }
	    for (auto& c : y1)
	    {
		c = tolower(c);
	    }
	    return x1 < y1; });

    for (const auto& i : vec)
    {
	cout << i << " ";
    }
    cout << endl;
}
