#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

vector<string> PalindromFilter(vector<string> words, int minLength)
{
    vector<string> result;
    for (auto w: words)
    {
	if (w.size() >= minLength)
	{
	    string r = w;
	    reverse(r.begin(), r.end());
	    if (r == w) {
		result.push_back(w);
	    }
	}
    }
    return result;
    
}

/*int main() {
    string s;
    do
    {
	cin >> s;
	cout << IsPalindrom(s);
    } while (s != "exit");
    }*/
