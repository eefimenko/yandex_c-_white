#include <iostream>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

int nod(int a_, int b_)
{
    int a = fabs(a_);
    int b = fabs(b_);
    
    while (a > 0 && b > 0) {
	if (a > b) {
	    a %= b;
	} else {
	    b %= a;
	}
    }
    return a + b;
}

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
	num = 0;
	den = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
	if (numerator != 0)
	{
	    int c = fabs(denominator);
	    int a = fabs(numerator);
	    int b = c;
	    
	    while (a > 0 && b > 0) {
		if (a > b) {
		    a %= b;
		} else {
		    b %= a;
		}
	    }
	    int nod = a + b;
	    int sign = denominator/c;
	    num = sign*numerator/nod;
	    den = c/nod;
	}
	else
	{
	    num = 0;
	    den = 1;
	}
    }
     

    int Numerator() const {
        // Реализуйте этот метод
	return num;
    }

    int Denominator() const {
        // Реализуйте этот метод
	return den;
    }

private:
    // Добавьте поля
    int num;
    int den;
};

// Реализуйте для класса Rational операторы ==, + и -
bool operator == (const Rational& lhs, const Rational& rhs)
{
    return (lhs.Numerator() == rhs.Numerator() && lhs.Denominator() == rhs.Denominator());
}

Rational operator - (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 - rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

Rational operator + (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 + rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

ostream& operator<< (ostream& out, const Rational& r)
{
    out << r.Numerator() << "/" << r.Denominator();
    return out;
}

istream& operator>> (istream& in, Rational& r)
{
    int a,b;
    if (in >> a && in.ignore(1) && in >> b)
    {
	r = Rational(a,b);
    }
    return in;
}

int main() {
    {
        ostringstream output;
        output << Rational(-6, 8);
        if (output.str() != "-3/4") {
            cout << "Rational(-6, 8) should be written as \"-3/4\"" << endl;
            return 1;
        }
    }

    {
        istringstream input("5/7");
        Rational r;
        input >> r;
        bool equal = r == Rational(5, 7);
        if (!equal) {
            cout << "5/7 is incorrectly read as " << r << endl;
            return 2;
        }
    }

    {
        istringstream input("5/7 10/8");
        Rational r1, r2;
        input >> r1 >> r2;
        bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << endl;
            return 3;
        }

        input >> r1;
        input >> r2;
        correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Read from empty stream shouldn't change arguments: " << r1 << " " << r2 << endl;
            return 4;
        }
    }

    cout << "OK" << endl;
    return 0;
}
