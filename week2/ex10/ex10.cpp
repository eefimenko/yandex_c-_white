#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
    int current_month = 0;
    vector<int> days = {31,28,31,30,31,30,31,31,30,31,30,31};
    vector<vector<string>> s(days[current_month]); // start with January
    
    int N; // number of operations
    cin >> N;
    
    for (int j = 0; j<N; j++)
    {
	string c; // command
	string t; // task
	int n; // number of day
	cin >> c; // read in command
	
	if (c == "DUMP")
	{
	    cin >> n; // if not count get the number of day
	    cout << s[n-1].size() << " ";
	    for (auto w: s[n-1])
	    {
		cout << w << " ";
	    }
	    cout << endl;
	}
	else if (c == "ADD")
	{
	    cin >> n; // if not count get the number of day
	    cin >> t; // get the task
	    s[n-1].push_back(t);
	}
	else if (c == "NEXT")
	{
	    int nd = days[(current_month+1)%12]; // number of days in the next month
	    int od = days[current_month]; // number of days in the current month
	    if (nd < od)
	    {
		for (int i=nd; i<od; i++)
		{
		    s[nd-1].insert(end(s[nd-1]), begin(s[i]), end(s[i]));
		}
	    }
	    current_month = (current_month + 1)%12;
	    s.resize(days[current_month]);
	}
    }
}
