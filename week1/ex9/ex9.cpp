#include <iostream>
#include <vector>
using namespace std;

int main() {
    int N;
    vector<int> res;
    cin >> N;
    while (N > 0)
    {
	int residual = N%2;
	res.push_back(residual);
	N /= 2; 
    }
    
    for (int i=res.size()-1; i>=0; i--)
    {
	cout << res[i];
    }
}
