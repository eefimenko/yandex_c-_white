#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Incognizable {
public:
    Incognizable() {
	a = 0;
	b = 0;
    };
    Incognizable(int a0)
    {
	a = a0;
	b = 0;
    };
    Incognizable(int a0, int b0)
    {
	a = a0;
	b = b0;
    };
private:
    int a;
    int b;
};

/*int main() {
    Incognizable a;
    Incognizable b = {};
    Incognizable c = {0};
    Incognizable d = {0, 1};
    }*/
