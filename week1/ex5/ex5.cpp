#include <iostream>
using namespace std;

int main() {
    double n,a,b,x,y;
    cin >> n >> a >> b >> x >> y;
    double s = n;
    double discount_x = 1. - x/100.;
    double discount_y = 1. - y/100.;
    
    if (a > b) {
	if (n > a)
	{
	    s *= discount_x;
	} else if (n > b) {
	    s *= discount_y;
	}
    } else {
	if (n > b)
	{
	    s *= discount_y;
	} else if (n > a) {
	    s *= discount_x;
	}
    }
    cout << s << endl;
}
