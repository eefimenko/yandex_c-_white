#include <string>
#include <vector>
#include <iostream>
using namespace std;

struct Person
{
    string name;
    string surname;
    int day;
    int month;
    int year;
};

int main() {
    int N,M;
    vector<Person> people;
    cin >> N;
 
    for (int i=0; i<N; i++)
    {
	Person p;
	cin >> p.name >> p.surname >> p.day >> p.month >> p.year;
	people.push_back(p);
    }

    cin >> M;
    for (int i=0; i<M; i++)
    {
	string command;
	int idx;
	int length = people.size();
	cin >> command >> idx;
	if (command == "name" && idx <= length && idx > 0)
	{
	    cout << people[idx-1].name << " " << people[idx-1].surname << endl; 
	} else if (command == "date" && idx <= length && idx > 0)
	{
	    cout << people[idx-1].day << "." << people[idx-1].month << "." << people[idx-1].year << endl; 
	}
	else
	{
	    cout << "bad request" << endl;
	}
    }
    return 0;
}

