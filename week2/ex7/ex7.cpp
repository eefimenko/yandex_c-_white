#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

vector<int> Reversed(const vector<int>& numbers)
{
    int n = numbers.size();
    vector<int> result(n);
    for (int i = 0; i < n; i++)
    {
	result[i] = numbers[n-i-1];
    }
    return result;
}

/*int main() {
    vector<int> a = {1,2,3,4,5};
    vector<int> a_r = Reverse(a);
    for (auto n: a_r)
    {
	cout << n << " ";
    }
    cout << endl;
    vector<int> b = {1,2,3,4};
    vector<int> b_r = Reverse(b);
    for (auto n: b_r)
    {
	cout << n << " ";
    }
    cout << endl;
    }*/
