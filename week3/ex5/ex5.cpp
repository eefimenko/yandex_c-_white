#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Person {
public:
    void ChangeFirstName(int year, const string& first_name) {
	name[year] = first_name;
    }
    void ChangeLastName(int year, const string& last_name) {
	surname[year] = last_name;
    }
    string GetFullName(int year) {
	string full_name,
	    name = FindName(year),
	    surname = FindSurname(year);
	
	if (name == "" && surname == "")
	{
	    full_name = "Incognito";
	}
	else if (name == "" && surname != "")
	{
	    full_name = surname + " with unknown first name";
	}
	else if (name != "" && surname == "")
	{
	    full_name = name + " with unknown last name";
	}
	else
	{
	    full_name = name + " " + surname;
	}
	return full_name;
    }
    string GetFullNameWithHistory(int year) {
	string full_name,
	    name = FindNameWithHistory(year),
	    surname = FindSurnameWithHistory(year);
	
	if (name == "" && surname == "")
	{
	    full_name = "Incognito";
	}
	else if (name == "" && surname != "")
	{
	    full_name = surname + " with unknown first name";
	}
	else if (name != "" && surname == "")
	{
	    full_name = name + " with unknown last name";
	}
	else
	{
	    full_name = name + " " + surname;
	}
	return full_name;
    }
private:
    map<int,string> name;
    map<int,string> surname;
    
    string FindName(int year)
    {
	string name = "";
	auto it = this->name.upper_bound(year);
	if (it != begin(this->name))
	{
	    it--;
	    name = it->second;; 
	}
	return name;
    }
    string FindSurname(int year)
    {
	string surname = "";
	auto it = this->surname.upper_bound(year);
	if (it != begin(this->surname))
	{
	    it--;
	    surname = it->second;; 
	}
	return surname;
    }
    
    string FindNameWithHistory(int year)
    {
	string name = "";
	vector<string> history;

	if (this->name.empty())
	{
	    return name;
	}

	string prev_name = name;
	for (const auto& s: this->name)
	{
	    if (s.first <= year)
	    {
		if (s.second != prev_name)
		{
		    prev_name = s.second;
		    history.push_back(s.second);
		}
	    }
	    else
	    {
		break;
	    }
	}
	
	name = prev_name;
	
	if (history.size() > 1)
	{
	    name += " (";
	    for (int k=history.size()-2; k > 0; k--)
	    {
		name += history[k] + ", ";
	    }
	    name += history[0] + ")";
	}
	return name;
    }

    string FindSurnameWithHistory(int year)
    {
	string surname = "";
	vector<string> history;

	if (this->surname.empty())
	{
	    return surname;
	}

	string prev_surname = surname;
	for (const auto& s: this->surname)
	{
	    if (s.first <= year)
	    {
		if (s.second != prev_surname)
		{
		    prev_surname = s.second;
		    history.push_back(s.second);
		}
	    }
	    else
	    {
		break;
	    }
	}
	
	surname = prev_surname;
	
	if (history.size() > 1)
	{
	    surname += " (";
	    for (int k=history.size()-2; k > 0; k--)
	    {
		surname += history[k] + ", ";
	    }
	    surname += history[0] + ")";
	}
	return surname;
    }
};

/*int main() {
    Person person;
    
   
    
    person.ChangeFirstName(1900, "Eugene");
    person.ChangeLastName(1900, "Sokolov");
    person.ChangeLastName(1910, "Sokolov");
    person.ChangeFirstName(1920, "Evgeny");
    person.ChangeLastName(1930, "Sokolov");
    cout << person.GetFullNameWithHistory(1940) << endl;   
    return 0;
    } */

