#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

void Reverse(vector<int>& numbers)
{
    int n = numbers.size();
    for (int i = 0; i < n/2; i++)
    {
	int tmp = numbers[i];
	numbers[i] = numbers[n-1-i];
	numbers[n-1-i] = tmp;
    }
}

/*int main() {
    vector<int> a = {1,2,3,4,5};
    Reverse(a);
    for (auto n: a)
    {
	cout << n << " ";
    }
    cout << endl;
    vector<int> b = {1,2,3,4};
    Reverse(b);
    for (auto n: b)
    {
	cout << n << " ";
    }
    cout << endl;
    }*/
