#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

bool IsPalindrom(string s)
{
    string r = s;
    reverse(r.begin(), r.end());
    if (r == s) {
	return true;
    }
    else
    {
	return false;
    }
}

/*int main() {
    string s;
    do
    {
	cin >> s;
	cout << IsPalindrom(s);
    } while (s != "exit");
    }*/
