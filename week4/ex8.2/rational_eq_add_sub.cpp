#include <iostream>
#include <math.h>
using namespace std;

int nod(int a_, int b_)
{
    int a = fabs(a_);
    int b = fabs(b_);
    
    while (a > 0 && b > 0) {
	if (a > b) {
	    a %= b;
	} else {
	    b %= a;
	}
    }
    return a + b;
}

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
	num = 0;
	den = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
	if (numerator != 0)
	{
	    int c = fabs(denominator);
	    int a = fabs(numerator);
	    int b = c;
	    
	    while (a > 0 && b > 0) {
		if (a > b) {
		    a %= b;
		} else {
		    b %= a;
		}
	    }
	    int nod = a + b;
	    int sign = denominator/c;
	    num = sign*numerator/nod;
	    den = c/nod;
	}
	else
	{
	    num = 0;
	    den = 1;
	}
    }
     

    int Numerator() const {
        // Реализуйте этот метод
	return num;
    }

    int Denominator() const {
        // Реализуйте этот метод
	return den;
    }

private:
    // Добавьте поля
    int num;
    int den;
};

// Реализуйте для класса Rational операторы ==, + и -
bool operator == (const Rational& lhs, const Rational& rhs)
{
    return (lhs.Numerator() == rhs.Numerator() && lhs.Denominator() == rhs.Denominator());
}

Rational operator - (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 - rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

Rational operator + (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 + rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

int main() {
    {
        Rational r1(4, 6);
        Rational r2(2, 3);
        bool equal = r1 == r2;
        if (!equal) {
            cout << "4/6 != 2/3" << endl;
            return 1;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a + b;
        bool equal = c == Rational(2, 1);
        if (!equal) {
            cout << "2/3 + 4/3 != 2" << endl;
            return 2;
        }
    }

    {
        Rational a(5, 7);
        Rational b(2, 9);
        Rational c = a - b;
        bool equal = c == Rational(31, 63);
        if (!equal) {
            cout << "5/7 - 2/9 != 31/63" << endl;
            return 3;
        }
    }
    const Rational r = Rational(1, 2) + Rational(-1, 2) - Rational(0, 1);
    if (r == Rational(0, 1)) {
	cout << "equal";
    }
    cout << "OK" << endl;
    return 0;
}
