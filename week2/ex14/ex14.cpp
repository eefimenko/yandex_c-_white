#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std;

int main() {
    int N; // number of pairs
    cin >> N;
   
    map<vector<string>, int> bus; // stops
        
    int number;
    
    for (int i = 0; i < N; i++)
    {
	cin >> number;
	vector<string> s(number);
	
	for (int k = 0; k < number; k++)
	{
	    string stop;
	    cin >> stop;
	    s[k] = stop;
	}
	
	if (bus.count(s) > 0)
	{
	    int i = bus.find(s)->second;
	    cout << "Already exists for " << i << endl;
	}
	else
	{
	    int i = bus.size() + 1;
	    bus[s] = i;
	    cout << "New bus " << i << endl;
	}
    }
}
