#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <math.h>
#include <iomanip>
#include <sstream>
#include <string>
using namespace std;

int nod(int a_, int b_)
{
    int a = fabs(a_);
    int b = fabs(b_);
    
    while (a > 0 && b > 0) {
	if (a > b) {
	    a %= b;
	} else {
	    b %= a;
	}
    }
    return a + b;
}

class Rational {
public:
    Rational() {
        // Реализуйте конструктор по умолчанию
	num = 0;
	den = 1;
    }

    Rational(int numerator, int denominator) {
        // Реализуйте конструктор
	if (numerator != 0)
	{
	    int c = fabs(denominator);
	    int a = fabs(numerator);
	    int b = c;
	    
	    while (a > 0 && b > 0) {
		if (a > b) {
		    a %= b;
		} else {
		    b %= a;
		}
	    }
	    int nod = a + b;
	    int sign = denominator/c;
	    num = sign*numerator/nod;
	    den = c/nod;
	}
	else
	{
	    num = 0;
	    den = 1;
	}
    }
     

    int Numerator() const {
        // Реализуйте этот метод
	return num;
    }

    int Denominator() const {
        // Реализуйте этот метод
	return den;
    }

private:
    // Добавьте поля
    int num;
    int den;
};

// Реализуйте для класса Rational операторы ==, + и -
bool operator == (const Rational& lhs, const Rational& rhs)
{
    return (lhs.Numerator() == rhs.Numerator() && lhs.Denominator() == rhs.Denominator());
}

Rational operator - (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 - rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

Rational operator + (const Rational& lhs, const Rational& rhs)
{
    int c = nod(rhs.Denominator(), lhs.Denominator());
    int d1 = lhs.Denominator()/c;
    int d2 = rhs.Denominator()/c;
    int a = lhs.Numerator() * d2 + rhs.Numerator() * d1;
    int b = d1*d2*c;
    return Rational(a,b);
}

ostream& operator<< (ostream& out, const Rational& r)
{
    out << r.Numerator() << "/" << r.Denominator();
    return out;
}

istream& operator>> (istream& in, Rational& r)
{
    int a,b;
    if (in >> a && in.ignore(1) && in >> b)
    {
	r = Rational(a,b);
    }
    return in;
}

bool operator< (const Rational& lhs, const Rational& rhs)
{
    Rational r = lhs - rhs;
    return r.Numerator() < 0;
}

int main() {
    {
        const set<Rational> rs = {{1, 2}, {1, 25}, {3, 4}, {3, 4}, {1, 2}};
        if (rs.size() != 3) {
            cout << "Wrong amount of items in the set" << endl;
            return 1;
        }

        vector<Rational> v;
        for (auto x : rs) {
            v.push_back(x);
        }
        if (v != vector<Rational>{{1, 25}, {1, 2}, {3, 4}}) {
            cout << "Rationals comparison works incorrectly" << endl;
            return 2;
        }
    }

    {
        map<Rational, int> count;
        ++count[{1, 2}];
        ++count[{1, 2}];

        ++count[{2, 3}];

        if (count.size() != 2) {
            cout << "Wrong amount of items in the map" << endl;
            return 3;
        }
    }

    cout << "OK" << endl;
    return 0;
}
