#include <iostream>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

int main() {
    vector<int> queue;
    int N; // number of operations
    cin >> N;
    
    for (int j = 0; j<N; j++)
    {
	string c; // command
	int n; // number
	cin >> c; // read in command
	
	if (c != "WORRY_COUNT")
	{
	    cin >> n; // if not count get the second parameter
	}

	if (c == "WORRY")
	{
	    queue[n] = 1;
	}
	else if (c == "QUIET")
	{
	    queue[n] = 0; 
	}
	else if (c == "COME")
	{
	    if (n > 0) // add quiet people
	    {
		for (int i = 0; i<n; i++)
		{
		    queue.push_back(0);
		}
	    }
	    else  // remove people from the end 
	    {
		for (int i = 0; i<abs(n); i++)
		{
		    queue.pop_back();
		}
	    }
	}
	else if (c == "WORRY_COUNT")
	{
	    int count = 0;
	    for (auto & q : queue)
	    {
		if (q == 1) count++;
	    }
	    cout << count << endl;
	}
	else
	{
	    cout << "Unknown command" << endl;
	    exit(-1);
	}
	/*for (auto& q : queue)
	{
	    cout << q << " ";
	}
	cout << endl;*/
    }
}
